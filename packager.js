'use strict';
var packager = require('electron-packager');
var options = {
    'arch': 'ia32',
    'platform': 'win32',
    'dir': './',
    'app-copyright': 'Emolitt',
    'app-version': '1.0.0',
    'asar': false,
    'icon': './icon.ico',
    'name': 'blindtest-generator',
    'out': './build',
    'overwrite': true,
    'prune': true,
    'version': '1.3.4',
    'version-string': {
        'CompanyName': 'Emolitt',
        'FileDescription': 'Generateur de blindtest', /*This is what display windows on task manager, shortcut and process*/
        'OriginalFilename': 'BlindtestGenerator',
        'ProductName': 'Blindtest Generator',
        'InternalName': 'BlindtestGeneratorDesktop'
    }
};
packager(options, function done_callback(err, appPaths) {
    console.log("Error: ", err);
    console.log("appPaths: ", appPaths);
});