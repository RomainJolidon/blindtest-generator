// Require moment.js
const moment = require('moment');
// Require ipcRender
const {ipcRenderer} = require('electron');
let jquery = require('jquery');

let timerTime;
let playlist;
let player = undefined;
let volume;
let pause = false;


// Helper function, to format the time
const secondsToTime = (s) => {
    let momentTime = moment.duration(s, 'seconds');
    let sec = momentTime.seconds();
    let min = momentTime.minutes();

    if (min <= 0) {
        return `${sec}`;
    }
    return `${min}:${sec}`;
};

function blink(){
    const f = document.getElementById('timerDiv');
    setInterval(function(){
        f.style.display = (f.style.display === 'none' ? '' : 'none');
    }, 1000);
}

function NextGuess() {
    playlist.shift();
}

async function LoadAudio() {
    player = await new Audio(playlist[0].music);
    player.volume = volume;
}

function LoadImage() {
}

function waitWhileAnswer(){
    const timerDiv = document.getElementById('timerDiv');
    let done = false;
    const screen = window.screen;
    const f = document.getElementById('answer');

    f.style.width = screen.width + 'px';
    f.style.height = screen.height + 'px';
    f.style.backgroundSize =  screen.width + 'px ' + screen.height + 'px'
    f.style.backgroundImage = 'url("' + playlist[0].image + '")';

    timerDiv.style.display = 'none';
    LoadImage();
    const answer = setInterval(function(){
        if (pause) {
            return;
        }
        if (!done) {
            done = !done;
        } else {
           timerDiv.style.display = '';
           f.style.backgroundImage = 'none';
            clearInterval(answer);
            NextGuess();
            player.pause();
            timer(timerTime);
        }
        //f.style.backgroundImage = (f.style.backgroundImage === 'none' ? 'url("./bg.png")' : 'none');
    }, 5000);
}

async function timer(t) {
    let currentTime = t;

    if (playlist.length <= 0) {
        stopPlaylist();
        return;
    }
    await LoadAudio().catch(e => {
        alert(e);
    });
    // Print out the time
    timerDiv.innerHTML = secondsToTime(currentTime);
    player.play();


    // Execute every second
    let timer = setInterval(() => {
        if (pause) {
            return;
        }
        // Remove one second
        currentTime = currentTime - 1;

        // Print out the time
        timerDiv.innerHTML = secondsToTime(currentTime);

        // When reaching 0. Stop.
        if (currentTime <= 0) {
            clearInterval(timer);
            waitWhileAnswer(player);
            //blink();
        }
    }, 1000); // 1 second
}

function stopPlaylist() {
    player.pause();
    playlist.length = 0;
    ipcRenderer.send('load-menu');
}

// Listen to the 'timer-change' event
ipcRenderer.on('start-playlist', (event, t, mplaylist) => {
    // Initialize time with value send with event
    timerTime = t;
    playlist = [...mplaylist];
    mplaylist.length = 0;
    volume = 0.5;

    //console.log(playlist)

    timer(t);
});

// Listen to the 'pause' event
ipcRenderer.on('toggle-pause', () => {
    pause = !pause;
    const f =  document.getElementById('pause-info');
    const time =  document.getElementById('timerDiv');
    f.innerHTML = (f.innerHTML === '' ? 'PAUSED' : '');
    if (!pause) {
        player.play();
        if (!document.getElementById('answer').style.backgroundImage.startsWith('url')) {
            time.style.display = '';
        }
    } else {
        player.pause();
        time.style.display = 'none';
    }
})

// Update 'volume'
ipcRenderer.on('update-volume', (event, gain) => {
    const newVolume = player.volume;

    if (newVolume + gain < 0 || newVolume + gain > 1) {
        return ;
    }
    player.volume += gain;
    volume = player.volume;
})

// Stop blindtest
ipcRenderer.on('stop-playlist', (event, gain) => {
    stopPlaylist();
})