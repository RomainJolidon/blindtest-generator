// Require moment.js
const moment = require('moment');
// Require ipcRender
const {ipcRenderer} = require('electron');
let jquery = require('jquery');

let timerTime;
let playlist;
let volume;
let pause = false;

// 2. This code loads the IFrame Player API code asynchronously.
const tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
let player;
let done = false;

function NewPlayer() {
    player = new YT.Player('player', {
        height: '1080',
        width: '1920',
        videoId: playlist[0].url, //fixme dans cet objet on peut rentrer l'id de la video
        playerVars: {
            start: playlist[0].start, //fixme ici on peut entrer le temps de départ en secondes
            rel: 0,
            controls: 0,
            showinfo: 0,
            iv_load_policy: 3
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
    done = false;
}

function onYouTubeIframeAPIReady() {
    //NewPlayer();
}

// 4.a. The API will call this function when the video player is ready.

function onPlayerReady(event) {
    event.target.setVolume(0);
    event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
function onPlayerStateChange(event) {
    if (event.data === YT.PlayerState.PLAYING && !done) {
        event.target.setVolume(volume);
        playGuess();
        done = true;
    }
    /*if (event.data === YT.PlayerState.PLAYING && !done) {
        setTimeout(stopVideo, 6000); //fixme dans ce timeout on peut regler la duree avant arret en millisecondes
        done = true;
    }*/
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function stopVideo() {
    player.setVolume(0);
    player.stopVideo();
    player.destroy();
    await sleep(1000);
}


// Helper function, to format the time
const secondsToTime = (s) => {
    let momentTime = moment.duration(s, 'seconds');
    let sec = momentTime.seconds();
    let min = momentTime.minutes();

    if (min <= 0) {
        return `${sec}`;
    }
    return `${min}:${sec}`;
};

function NextGuess() {
    stopVideo();
    playlist.shift();
    if (playlist.length > 0) {
        NewPlayer();
    } else {
        stopPlaylist();
    }
}

function waitWhileAnswer(callback){
    const timerDiv = document.getElementById('timerDiv');
    let done = false;
    const screen = window.screen;
    const f = document.getElementById('player');

    f.style.width = screen.width + 'px';
    f.style.height = screen.height - 10 + 'px';
    f.style.backgroundSize =  screen.width + 'px ' + screen.height - 10 + 'px'
    f.style.display = '';

    timerDiv.style.display = 'none';
    const answer = setInterval(function(){
        if (pause) {
            return;
        }
        if (!done) {
            done = !done;
        } else {
            timerDiv.style.display = '';
            timerDiv.innerHTML = secondsToTime(timerTime);
            f.style.display = 'none';
            clearInterval(answer);
            callback();
        }
        //f.style.backgroundImage = (f.style.backgroundImage === 'none' ? 'url("./bg.png")' : 'none');
    }, 5000);
}

function timer(t, callback) {
    let currentTime = t;

    if (playlist.length <= 0) {
        stopPlaylist();
        return;
    }

    // Print out the time
    timerDiv.innerHTML = secondsToTime(currentTime);

    console.log("on va demarrer le timer");
    // Execute every second
    let timer = setInterval(() => {
        if (pause) {
            return;
        }
        // Remove one second
        currentTime = currentTime - 1;
        console.log(currentTime);

        // Print out the time
        timerDiv.innerHTML = secondsToTime(currentTime);

        // When reaching 0. Stop.
        if (currentTime <= 0) {
            clearInterval(timer);
            callback();
            //blink();
        }
    }, 1000);// 1 second
}

function stopPlaylist() {
    playlist.length = 0;
    ipcRenderer.send('load-menu');
}

function playGuess() {
    if (playlist.length > 0) {
        timer(timerTime, () => {
            waitWhileAnswer(() => {
                NextGuess();
            });
        });
    } else {
        stopPlaylist();
    }
}

// Listen to the 'timer-change' event
ipcRenderer.on('start-playlist', async (event, t, mplaylist) => {
    // Initialize time with value send with event
    timerTime = t;
    playlist = [...mplaylist];
    mplaylist.length = 0;
    volume = 50;


    //console.log(playlist)

    NewPlayer();

    //stopPlaylist();
});

// Listen to the 'pause' event
ipcRenderer.on('toggle-pause', () => {
    pause = !pause;
    const f =  document.getElementById('pause-info');
    const time =  document.getElementById('timerDiv');
    f.innerHTML = (f.innerHTML === '' ? 'PAUSED' : '');
    if (!pause) {
        player.playVideo();
        console.log(document.getElementById('player').style.width);
        if (document.getElementById('player').style.width === '') {
            time.style.display = '';
        }
    } else {
        player.pauseVideo();
        time.style.display = 'none';
    }
})

// Update 'volume'
ipcRenderer.on('update-volume', (event, gain) => {

    console.log('upade volume: ' + gain);
    if (volume + gain < 0 || volume + gain > 100) {
        return ;
    }
    player.setVolume(volume + gain);
    volume = player.getVolume();
})

// Stop blindtest
ipcRenderer.on('stop-playlist', (event, gain) => {
    stopPlaylist();
})