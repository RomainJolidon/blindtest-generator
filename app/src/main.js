const electron = require('electron');
const { app, BrowserWindow, dialog, screen} = require('electron');
const ipcMain = electron.ipcMain;
const generator = require('./utils/SelectionGenerator');

let mainWindow;

let timerTime = 3;
let fullscreenStatus = false;

function InitializeKeybinds() {
    // When UI has finish loading
    mainWindow.webContents.on("before-input-event", (event, input) => {
        if (input.type === 'keyDown') {
            return;
        }
        switch (input.key) {
            case ' ':
                mainWindow.webContents.send('toggle-pause');
                console.log('pause toggled');
                break;
            case 'F11':
                fullscreenStatus = !fullscreenStatus;
                mainWindow.setFullScreen(fullscreenStatus);
                break;
            case 'Escape':
                if (fullscreenStatus === false) {
                    mainWindow.webContents.send('stop-playlist');
                }
                fullscreenStatus = false;
                mainWindow.setFullScreen(fullscreenStatus);
                break;
            case 'ArrowUp':
                mainWindow.webContents.send('update-volume', 10);
                break;
            case 'ArrowDown':
                mainWindow.webContents.send('update-volume', -10);
                break;
            default:
                break;
        }
    });
}

function createWindow () {

    const { width, height } = screen.getPrimaryDisplay().workAreaSize;
    mainWindow = new BrowserWindow({
        title: 'Blindtest Generator',
        center: true,
        icon: "/resources/icon.ico",
        width: width,
        height: height,
        frame: true,
        fullscreen: false,
        webPreferences: {
            nodeIntegration: true
        }
    }); // on définit une taille pour notre fenêtre

    //gitlab.git();

    InitializeKeybinds();

    mainWindow.removeMenu();
    mainWindow.maximize();
    //mainWindow.webContents.openDevTools();

    mainWindow.loadFile('src/menu/menu.html'); // on doit charger un chemin absolu

    mainWindow.on('closed', () => {
        mainWindow = null;
    });

}

//app.allowRendererProcessReuse = false;

// Quitter si toutes les fenêtres ont été fermées.
app.on('window-all-closed', () => {
    // Sur macOS, il est commun pour une application et leur barre de menu
    // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
    // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
    if (mainWindow === null) {
        createWindow()
    }
})
// Dans ce fichier, vous pouvez inclure le reste de votre code spécifique au processus principal. Vous pouvez également le mettre dans des fichiers séparés et les inclure ici.

ipcMain.on('close-window', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

ipcMain.on('load-menu', () => {
    mainWindow.loadFile(`src/menu/menu.html`); // on doit charger un chemin absolu
});

ipcMain.on('start-blindtest', (event, config) => {
    //on recupere la config genere
    //on genere une playlist en fonction de la config
    //const playlist = generator.GenerateSelection(config.path, config.size);
    const playlist = generator.GenerateSelectionFromJSON(config.path, config.size);

    mainWindow.loadFile(`src/renderer/test.html`); // on doit charger un chemin absolu
    fullscreenStatus = true;
    mainWindow.setFullScreen(true);
    mainWindow.webContents.on('did-finish-load', () => {
        // Send the timer value
        mainWindow.webContents.send('start-playlist', config.guessTime, playlist);
    });
});

ipcMain.on('select-assets', async () => {
    let options = {
        properties:["openFile"],
        filters: [
            { name : 'Assets JSON', extensions: ['json'] }
        ]
    }

    //Synchronous
    let dir = await dialog.showOpenDialog(options);
    if (!dir.canceled) {
        mainWindow.webContents.send('update-path-value', dir.filePaths[0]);
    }
})


// Cette méthode sera appelée quand Electron aura fini
// de s'initialiser et prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quand cet événement est émit.
app.whenReady().then(createWindow)