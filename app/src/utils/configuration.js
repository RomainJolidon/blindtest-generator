//Blindtest Configuration object. Default is None
let guessTime;
let theme;
let size;

function SetConfiguration(mguessTime, mtheme, msize) {
    guessTime = mguessTime
    theme = mtheme;
    size = msize;
    console.log('Configuration set:\n', + theme);
}

function GetConfiguration() {
    return (configuration);
}

module.exports = {
    SetConfiguration: SetConfiguration,
    GetConfiguration: GetConfiguration
}