const config = require('../utils/configuration');
const {ipcRenderer, dialog} = require('electron');
const generator = require('../utils/SelectionGenerator');

$(document).ready(function() {

    const $valueSpan = $('.guessTimeValue');
    const $value = $('#guess-time-slider');
    const assetsPath = $('dirs');
    $valueSpan.html($value.val());
    $value.on('input change', () => {

        /*if (parseInt($value.val()) < 10) {
            $valueSpan.html('0' + $value.val());
        } else {
            $valueSpan.html($value.val());
        }*/
        $valueSpan.html($value.val());
    });
});

$('#start-button').on('click', function(){
    const guessTime = $('#guess-time-slider').val();
    const theme = $('#theme-selector').val();
    const size = $('#playlist-size').val();

    const configData = {
        guessTime: parseInt(guessTime),
        theme: theme,
        size: parseInt(size)
    };
    console.log(configData);
    config.SetConfiguration(guessTime, theme, size);
});

document.getElementById('start-button').addEventListener('click', () => {
    const guessTime = $('#guess-time-slider').val();
    const theme = $('#theme-selector').val();
    const size = $('#playlist-size').val();
    const path = document.getElementById('assetsPath').innerText;

    if (path === 'None') {
        alert('Path to assets not set');
        return ;
    }
    const configData = {
        guessTime: parseInt(guessTime),
        theme: theme,
        size: parseInt(size),
        path: path
    };
    ipcRenderer.send('start-blindtest', configData);
});


document.getElementById('dirs').addEventListener('click', () => {
   ipcRenderer.send('select-assets');
});

// Listen to the 'timer-change' event
ipcRenderer.on('update-path-value', (event, value) => {
    const $valueSpan = $('#assetsPath');
    //const validated = generator.CheckAssetsDirectory(value)
    const validated = undefined;
    if (validated !== undefined) {
        alert('Invalid Assets Directory: ' + validated);
        $valueSpan.html('None');
    } else {
        $valueSpan.html(value);
    }
});
